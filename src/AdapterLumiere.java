import thermos.Thermostat;

/**
* @author HOU Yunan
* @version 2018年5月15日 上午8:47:54
*/
public class AdapterLumiere extends Appareil{
	  Lumiere m;
	  public AdapterLumiere(String s){
	    super(s);
	    this.m=new Lumiere();
	  }
	  public void allumer(){
	    this.m.changerIntensite(this.m.getLumiere()+10);
	  }
	  public void eteindre(){
	    this.m.changerIntensite(0);
	  }
	  public String toString(){
	    return this.m.toString();
	  }
}
