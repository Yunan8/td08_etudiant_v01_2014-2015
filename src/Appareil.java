
public abstract class Appareil {

	/**
	 * nom de la lampe
	 */
	String nom;

	public Appareil(String paramNom){
		this.nom = paramNom;
	}



	public abstract void allumer() ;
	public abstract void eteindre() ;
	public abstract String toString() ;

}
