import thermos.Thermostat;


public class Main {

	@SuppressWarnings("unused")
	public static void main(String args[])
	{
		Telecommande t=new Telecommande();
		
		
		Lampe l1=new Lampe("Lampe1");
		t.ajouterAppareil(l1);
		
		AdapterLumiere a1=new AdapterLumiere("lumiere1");
		t.ajouterAppareil(a1);
		
		Hifi h1=new Hifi("Hifi1");
		t.ajouterAppareil(h1);
		
		
		AdapterThermostat t1=new AdapterThermostat("thermostat");
		t.ajouterAppareil(t1);
		
		
		TelecommandeGraphique tg=new TelecommandeGraphique(t);
		

		
	}
	
}
