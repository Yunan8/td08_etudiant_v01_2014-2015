import java.util.ArrayList;
import java.util.List;




public class Telecommande {
	
	private List<Appareil> objets;
	
	public Telecommande(){
		this.objets=new ArrayList<Appareil>();
		
	}
	
	public void ajouterAppareil(Appareil l){
		this.objets.add(l);
	}
	
	
	public void activerAppareil(int indice){
		if(indice < this.objets.size() && indice>=0){
			this.objets.get(indice).allumer();
		}
		
	}
	
	
	public void desactiverAppareil(int indice){
		if(indice < this.objets.size() && indice>=0){
			this.objets.get(indice).eteindre();
		}
	}
	
	public void activerTout(){
		for(int i=0;i<objets.size();i++){
			this.objets.get(i).allumer();
		}
	}
	
	public String toString(){
		String s="";
		for(int i=0;i<objets.size();i++){
			s=s+this.objets.get(i).toString()+"\n";
		}
		return s;
	}
	public int getNombre() {
		return this.objets.size();
	}

}
