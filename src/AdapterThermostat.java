import thermos.Thermostat;

public class AdapterThermostat extends Appareil{
  
  Thermostat t;
  public AdapterThermostat(String s){
    super(s);
    this.t=new Thermostat();
  }
  public void allumer(){
	for(int i=0;i<10;i++){
		   this.t.monterTemperature();
	}
 
  }
  public void eteindre(){
	  for(int i=0;i<10;i++){
		   this.t.baisserTemperature();
	}
  }
  public String toString(){
    return "Thermostat";
  }
}
