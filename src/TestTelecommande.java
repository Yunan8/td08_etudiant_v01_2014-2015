import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
* @author HOU Yunan
*
*/
public class TestTelecommande {
	@Test
	public void TestConstructeur1() {
		Telecommande t=new Telecommande();
		Lampe l=new Lampe("test");

		t.ajouterAppareil(l);
		assertEquals ("ajout d'une lampe à une télécommande vide ",t.toString(),"test: Off\n");
	}
	@Test
	public void TestConstructeur2() {
		Telecommande t=new Telecommande();
		Lampe l=new Lampe("test");

		t.ajouterAppareil(l);
		t.ajouterAppareil(l);
		t.ajouterAppareil(l);
		assertEquals (" ajout d'une lampe à une télécommande avec 1 élément ",t.toString(),"test: Off\ntest: Off\ntest: Off\n");
	}
	@Test
	public void TestConstructeur3() {
		Telecommande t=new Telecommande();
		AdapterLumiere l=new AdapterLumiere("test");

		t.ajouterAppareil(l);
		t.ajouterAppareil(l);
		t.ajouterAppareil(l);
		assertEquals (" ajout d'une lampe à une télécommande avec 1 élément ",t.toString(),"lumiere: 0\nlumiere: 0\nlumiere: 0\n");
	}
	@Test
	public void TestActivation1() {
		Telecommande t=new Telecommande();
		Lampe l=new Lampe("test");

		t.ajouterAppareil(l);
		t.activerAppareil(0);
		assertEquals ("ajout d'une lampe à une télécommande vide ",t.toString(),"test: On\n");
	}
	@Test
	public void TestActivation2() {
		Telecommande t=new Telecommande();
		Lampe l=new Lampe("test");
		Lampe m=new Lampe("test2");
		t.ajouterAppareil(l);
		t.ajouterAppareil(m);
		t.activerAppareil(1);
		assertEquals ("ajout d'une lampe à une télécommande vide ",t.toString(),"test: Off\ntest2: On\n");
	}
	@Test
	public void TestActivation3() {
		Telecommande t=new Telecommande();
		t.activerAppareil(0);
	}
	@Test
	public void TestActivation4() {
		Telecommande t=new Telecommande();
	    AdapterLumiere l=new AdapterLumiere("test");

		t.ajouterAppareil(l);
		t.activerAppareil(0);
		assertEquals ("ajout d'une lampe à une télécommande vide ",t.toString(),"lumiere: 10\n");
	}
	@Test
	public void TestActivation5() {
		Telecommande t=new Telecommande();
	    AdapterLumiere l=new AdapterLumiere("test");

		t.ajouterAppareil(l);
		t.activerAppareil(0);
		t.desactiverAppareil(0);
		assertEquals ("ajout d'une lampe à une télécommande vide ",t.toString(),"lumiere: 0\n");
	}
}
